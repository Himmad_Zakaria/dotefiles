rsync -a ~/.config/sublime-text-2/Packages/User/snippets/*  sublime_text_2/snippets
rsync -a ~/.config/sublime-text-2/Packages/User/Preferences.sublime-settings sublime_text_2/Preferences.sublime-settings
rsync ~/.zshrc zsh/zshrc
rsync ~/.oh-my-zsh/custom/plugins/common-aliases/common-aliases.plugin.zsh  oh-my-zsh/common-aliases.plugin.zsh
rsync ~/.tilda/config_* tilda/
rsync  ~/.vimrc vim/vimrc
rsync -a ~/.vim/colors/ vim/colors/
rsync -a ~/.config/terminator terminator/
rsync ~/.conkyrc conkyrc
cp -R  sublime_text_2/snippets ~/.config/sublime-text-2/Packages/User/snippets/*
cp -R sublime_text_2/Preferences.sublime-settings ~/.config/sublime-text-2/Packages/User/Preferences.sublime-settings 
cp  zsh/zshrc ~/.zshrc 
cp oh-my-zsh/common-aliases.plugin.zsh ~/.oh-my-zsh/custom/plugins/common-aliases/common-aliases.plugin.zsh  
cp tilda/* ~/.tilda/
cp  vim/vimrc ~/.vimrc 
cp vim/colors/ ~/.vim/colors/ 
cp -R terminator/ ~/.config/terminator
cp conkyrc ~/.conkyrc 